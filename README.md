# Application blueprint starter template

Use this project template to create an application blueprint. 

It contains the following files:

``ensemble-template.yaml``: This will be included by the ensemble created when a user deploys your blueprint.

``unfurl.json``: This file is automatically re-generated every time a change is committed to ``ensemble-template.yaml``.  

 